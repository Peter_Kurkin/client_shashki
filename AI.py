import random
import socket
import time

fields = ["a", "b", "c", "d", "e", "f", "g", "h"]


def create_list_board(data, read_file) -> list[list[str]]:
    board = []
    while data:
        k = 0
        if "|" in data:
            result = data.split(" | ")
            del result[0]
            board.append(result)
        k += 1
        data = read_file.readline().strip()
    return board


def read_data(read_file):
    data = read_file.readline().strip()
    while data:
        data = read_file.readline().strip()


def jump(write_file, board, read_file) -> None:
    x = 0
    for line in board:
        y = 0
        for _ in line:
            write_file.write(f"jump {fields[x]}{y} {fields[(x - 2) % 8]}{y - 2}" + " \n")
            write_file.flush()
            read_data(read_file)

            write_file.write(f"jump {fields[x]}{y} {fields[(x - 2) % 8]}{y + 2}" + " \n")
            write_file.flush()
            read_data(read_file)

            write_file.write(f"jump {fields[x]}{y} {fields[(x + 2) % 8]}{y + 2}" + " \n")
            write_file.flush()
            read_data(read_file)

            write_file.write(f"jump {fields[x]}{y} {fields[(x + 2) % 8]}{y - 2}" + " \n")
            write_file.flush()
            read_data(read_file)

            y += 1
        x += 1


def get_figure(board, color) -> str:
    while True:
        letter = random.choice(fields)
        num = random.randint(0, 7)
        if color in board[ord(letter) % ord(fields[0])][num]:
            return letter + str(num)


def move(write_file, read_file, board):
    while True:
        jump(write_file, board, read_file)
        figure_letter = random.choice(fields)
        figure_num = random.randint(0, 7)
        for i in range(64):
            letter = random.choice(fields)
            num = str(random.randint(1, 8))
            write_file.write(f"move {figure_letter + str(figure_num)} {letter + num}" + " \n")
            write_file.flush()
            read_data(read_file)


def client():
    host = input("HOST: ")
    port = int(input("PORT"))
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))
        read_file = s.makefile(mode="r", encoding="utf-8")
        write_file = s.makefile(mode="w", encoding="utf-8")
        count = 0
        while True:
            read_data(read_file)
            if count == 0:
                count += 1
                write_file.write("create_room 1" + " \n")
                write_file.flush()
            elif count == 1:
                count += 1
                write_file.write("my_info" + " \n")
                write_file.flush()
                my_info = read_file.readline().strip()[4:]
            elif count == 2:
                count += 1
                write_file.write("start_game" + " \n")
                write_file.flush()
                data = read_file.readline().strip()
                while 'There are not enough players in the room.' in data or not data:
                    write_file.write("start_game" + " \n")
                    write_file.flush()
                    data = read_file.readline().strip()
                    print(data)
                    if '#' in data:
                        break
                    time.sleep(2)
                if my_info in data:
                    color = 'w'
                else:
                    color = 'b'
                move(write_file, read_file, create_list_board(data, read_file))


if __name__ == "__main__":
    client()
