import socket


def client():
    host = input("HOST: ")
    port = int(input("PORT"))
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))
        read_file = s.makefile(mode="r", encoding="utf-8")
        write_file = s.makefile(mode="w", encoding="utf-8")
        while True:
            data = read_file.readline().strip()

            while data:
                if "+" in data:
                    data = '   ' + data
                elif "1" in data:
                    data = '      ' + data
                print(data)
                data = read_file.readline().strip()

            a = input("send: ")
            if a == "exit":
                break
            write_file.write(a + " \n")

        write_file.flush()
    s.close()


if __name__ == "__main__":
    client()
